package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

public class GameScreen extends ScreenAdapter {
	private TabGame tabGame;

	private Texture tabImg;
	private Texture backgroundImg;
	private Texture buttonImg;
	private Texture pressedImg;
	private Texture slotlineImg;
	private Texture fillslotImg;
	private Texture finImg;
	
	private ArrayList<Tab> tabs;
	private Tab tab;
	private ArrayList<Button> buttons;
	private Button button;
	private boolean []presses;
	private Music music;
	private Sound sound;
	
	public static final int SLOT = 6;
	public static final int SLOT_0 = 0;
	public static final int SLOT_1 = 1;
	public static final int SLOT_2 = 2;
	public static final int SLOT_3 = 3;
	public static final int SLOT_4 = 4;
	public static final int SLOT_5 = 5;
	public static final int SPEED = 3;
	
	private BitmapFont font;
	
	long timer_wave = TimeUtils.millis();
	long timer_fall = TimeUtils.millis();
	long timer_score = TimeUtils.millis();
	long timer_music = TimeUtils.millis();
	long timer_delay_play = TimeUtils.millis();
	
	Vector2 pos;
	long score;
	double status;
	
	boolean is_anyone_decrease = false;
	boolean first_time_play_music = true;
	boolean allow_update_sound = true;
	boolean fin = false;
	
	int state_wave = 0;
	int state_music = 0;
	long bad_status = 0;
	long id;
	
	public GameScreen(TabGame tabGame) {
		
		this.tabGame = tabGame;
		
		tabImg = new Texture("tab_1.png");
		backgroundImg = new Texture("background_0.jpg");
		buttonImg = new Texture("button.png");
		pressedImg = new Texture("pressedbutton.png");
		slotlineImg = new Texture("tab.png");
		fillslotImg = new Texture("fillslot.png");
		finImg = new Texture("fin.jpg");
		
		tabs = new ArrayList<Tab>();
    	buttons = new ArrayList<Button>();
    	presses = new boolean[SLOT];
    	
    	music = new Music();
    	tab = new Tab(0, 0, 0);
    	button = new Button(0, 0);
    	
    	font = new BitmapFont();
    	
    	setbutton();
    	
    	set_music();
    	id = sound.play(1.0f);
    	sound.stop(id);
    	
    }
	
	@Override
    public void render(float delta) {
		
		control_begin();
		
		update();
		
		SpriteBatch batch = tabGame.batch;
		
		batch.begin();
		
		render_reset();
		render_object(batch);
		
        batch.end();
	
		control_end();
		
		
		
	} 
	
	private void control_begin(){
		 if(Gdx.input.isKeyPressed(Keys.A)) {
	            presses[0] = true;
	     }
		 if(Gdx.input.isKeyPressed(Keys.S)) {
	            presses[1] = true;
	     }
		 if(Gdx.input.isKeyPressed(Keys.D)) {
	            presses[2] = true;
	     }
		 if(Gdx.input.isKeyPressed(Keys.J)) {
	            presses[3] = true;
	     }
		 if(Gdx.input.isKeyPressed(Keys.K)) {
	            presses[4] = true;
	     }
		 if(Gdx.input.isKeyPressed(Keys.L)) {
	            presses[5] = true;
	     }
	}
	
	private void control_end(){
		for(int slot = 0;slot < SLOT;slot++){
			presses[slot] = false;
		}
	}
	
	private void update(){
		if(TimeUtils.millis() - timer_wave > 300){
			update_spawn();
			timer_wave = TimeUtils.millis();
		}
		if(TimeUtils.millis() - timer_fall > 30){
			update_tab();
			timer_fall = TimeUtils.millis();
		}
		if(TimeUtils.millis() - timer_score > 0){
			update_score();
			update_status();
			timer_score = TimeUtils.millis();
		}
		if(first_time_play_music == true || state_wave == music.music(state_music).length - 1 ){
			update_sound();
			
		}
			update_remove();
	
	}
	
	private void update_spawn(){
		wave_spawn(music.music(state_music));
		state_wave++;
	}

	private void wave_spawn(String[] music){
		if(state_wave < music.length){
			run(music);
		}
	}
	
	private void run(String[] music) {
			for(int slot = 0;slot < SLOT;slot++){
				if(music[state_wave].charAt(slot) == '_'){
					spawn(slot);
				}
			}
	}

	private void update_tab() {
		for(Tab t:tabs){	
			Vector2 pos = t.getPosition();
			pos.y -= SPEED;
		}
	}

	private void update_score(){
		for(int slot = 0;slot < SLOT;slot++){
			Vector2 pos_button = buttons.get(slot).getPosition();
			is_anyone_decrease = false;
			
			for(Tab t:tabs){
				Vector2 pos_tab = t.getPosition();
			
				if(presses[slot] == true && t.getSlot() == slot){
		
					cal_score(pos_button, pos_tab, tabs, slot);
				
				}
			}
		}
	}
	
	private void cal_score(Vector2 pos_button, Vector2 pos_tab, ArrayList<Tab> tabs2, int slot2){
		if(is_anyone_touching(tabs, pos_button, slot2)){
			if(is_touching(pos_button, pos_tab)){
				score++;
			}
		}
		else{
			if(is_anyone_decrease == false){
				bad_status++;
				is_anyone_decrease = true;
			}
		}
	}
	
	private boolean is_anyone_touching(ArrayList<Tab> tabs2, Vector2 pos_button, int slot2) {
		boolean is_touched = false;
		for(Tab t:tabs){
			Vector2 pos_tab = t.getPosition();
			if(t.getSlot() == slot2 && is_touching(pos_button, pos_tab)){
				t.press();
				is_touched = true;
			}
		}
		return is_touched;
	}

	private boolean is_touching(Vector2 pos_button, Vector2 pos_tab) {
		if(distance(pos_button, pos_tab) < button.getHeight()/2.0 + tab.getHeight()/2.0){
			return true;
		}
		else{
			return false;
		}
	}

	private double distance(Vector2 pos_button, Vector2 pos_tab) {
		double distance_x = Math.abs(pos_button.x - pos_tab.x);
		double distance_y = Math.abs(pos_button.y - pos_tab.y);
		return Math.pow(Math.pow(distance_x, 2) + Math.pow(distance_y, 2), 0.5);
	}
	
	private void update_status(){
		status = (bad_status+100.1) / (score+800.1);
	}
	
	private void update_sound(){
		
		
		if(allow_update_sound == true){
			
    		
			if(first_time_play_music == false){
				if(state_music < 3 - 1){
					sound.stop(id);
					state_music++;
					state_wave = 0;
				}
				else{
					allow_update_sound = false;
					fin = true;	
				}
				
    		}
			if(allow_update_sound == true){
				set_music();
				id = sound.play(1.0f);
			}
			
			first_time_play_music = false;
		}
	}
	
	private void set_music(){
		sound = Gdx.audio.newSound(Gdx.files.internal(music.list_of_musics[state_music] + ".MP3"));
	}
	
	private void update_remove(){
		for(int num = 0;num < tabs.size();num++){
			Vector2 pos = tabs.get(num).getPosition();
			if(pos.y < 0){
				if(!(tabs.get(num).getTouched())){
					score -= 100;
				}
				tabs.remove(num);
			}
		}
	}
	
	private void render_reset(){
		Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}
	
	private void render_object(SpriteBatch batch){
		render_background(batch);
		render_fillslot(batch);
		render_tab(batch);
		render_button(batch);
		render_score(batch);
		render_slotline(batch);
		render_status(batch);
		render_stage(batch);
		render_fin(batch);
	}
	
	private void render_background(SpriteBatch batch) {
		batch.draw(backgroundImg, 0, 0, 1366, 768);
		
	}

	private void render_fillslot(SpriteBatch batch){
		for(int slot = 0;slot < SLOT;slot++){
			if(presses[slot] == true){
				batch.draw(fillslotImg, 50 + 2 + slot * 200, 0, 200, 768);
			}
		}
	}
	
	private void render_tab(SpriteBatch batch){
		for(Tab t:tabs){
			pos = t.getPosition();
			if(pos.y < 768 - 200 * state_music){
				batch.draw(tabImg, pos.x, pos.y, tab.getWidth(), tab.getHeight());
			}
		}
	}
    	
	
	private void render_button(SpriteBatch batch){
		
		for(int slot = 0;slot < SLOT;slot++){
			pos = buttons.get(slot).getPosition();
			if(presses[slot] == true) {
				batch.draw(pressedImg, pos.x, pos.y, button.getWidth(), button.getHeight());
			}
		
			else{
				batch.draw(buttonImg, pos.x, pos.y, button.getWidth(), button.getHeight());
			}
		}
	}
	
	private void render_score(SpriteBatch batch){
		font.draw(batch,"Score:", 1270, 720);
		font.draw(batch,"" + score, 1270, 700);
	}
	
	private void render_slotline(SpriteBatch batch){
		for(int slot = 0;slot <= SLOT;slot++){
			batch.draw(slotlineImg, 50 + slot * 200, 0, 2, 768);
		}
	}
	
	private void render_status(SpriteBatch batch){
		font.draw(batch,"Status:", 1270, 660);
		font.draw(batch,"" + status, 1270, 640);
		font.draw(batch,status(status), 1270, 620);
	}
	
	private String status(double status){
		if(status < 0)			return "Be attention";
		else if(status < 0.01)	return "Perfect Pitch";
		else if(status < 0.05)	return "Smart";
		else if(status < 0.10)	return "Good Looking";
		else if(status < 0.15)	return "Fine";
		else if(status < 0.20)	return "So So";
		else					return "Be deaf";
	}

	private void render_stage(SpriteBatch batch){
		font.draw(batch,"state_music", 1270, 580);
		font.draw(batch,"" + state_music, 1270, 560);
	}
	
	private void render_fin(SpriteBatch batch){
		if(fin){
			batch.draw(finImg, 0, 0, 1366, 768);      
		}
	}
	
	private void spawn(int slot){
		tabs.add(new Tab(100 + slot * 200, 720, slot));
	}
	
	private void setbutton(){
		for(int slot = 0;slot < SLOT;slot++){
			buttons.add(new Button(100 + slot * 200, 50));
		}
	}

}
