package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;

public class Tab {
	private Vector2 position;
	private int slot;
	private int width = 100;
	private int height = 30;
	private boolean touched = false; 
	 
    public Tab(int x, int y, int in_slot) {
        position = new Vector2(x,y);
        slot = in_slot;
    }    
 
    public Vector2 getPosition() {
        return position;    
    }

    public int getSlot() {
        return slot;    
    }
    
    public int getWidth() {
        return width;    
    }
    
    public int getHeight() {
        return height;    
    }

    public boolean getTouched(){
    	return touched;
    }
    
    public void press(){
    	touched = true;
    }
    
}
