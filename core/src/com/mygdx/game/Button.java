package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;

public class Button {
	private Vector2 position;
	private int width = 100;
	private int height = 30;
		 
    public Button(int x, int y) {
        position = new Vector2(x,y);
    }    
 
    public Vector2 getPosition() {
        return position;    
    }
    
    public int getWidth() {
        return width;    
    }
    
    public int getHeight() {
        return height;    
    }
}
